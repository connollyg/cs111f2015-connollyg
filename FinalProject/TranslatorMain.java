//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Final Project
// Date: mm dd yyyy
//
// Purpose: To convert simple English statements into German.
//********************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;
import java.io.File;

public class TranslatorMain
{
    //------------------
	//main method: program execution begins here
	//------------------
    static final String ENGLISH = "English.txt";
    static final String GERMAN = "German.txt";

    public static void main(String[] args)
    {
	  // Label output with name and date:
        System.out.println("Grace Connolly\nFinal Project\n" + new Date() + "\n");

        System.out.println("Welcome to the English to German translator!");

        Scanner scan = new Scanner(System.in);
        Filez filez = new Filez(new File(ENGLISH));
        ComparePhrases comparephrases = new ComparePhrases();

        System.out.println("Please enter simple phrase in English.");
        System.out.println("Example: 'My name is' or 'I like to read'");
        String input = scan.nextLine();

        String translated = comparephrases.thereItIs(input);

        System.out.println("Here is your translation: " +translated);
        System.out.println("Thank you for using this translator!");

    }
}
