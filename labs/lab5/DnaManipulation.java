
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Deyalyn Batista and Grace Connolly
// CMPSC 111 Fall 2015
// Lab #5
// Date: 25 September 2015
//
// Purpose:To create a program that mutates DNA
//********************************
import java.util.Date; // needed for printing today's date<Plug>(neosnippet_expand)
import java.util.Random;
import java.util.Scanner;


public class DnaManipulation
{
	//------------------
	//main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{
	  // Label output with name and date:
        System.out.println("Grace Connolly and Deyalyn Batista\nLab #5\n" + new Date() + "\n");

        // Variable dictionary:
        Scanner scan = new Scanner(System.in); // used for input
        Random r = new Random(); // random number generator
        String dnaString, dnaString2, dnaString3, dnaString4, dnaString5, dnaString6, dnaString7, dnaString8; // s1 = input by user, s2 = used in computations
        int len;       // length of string s1
        int location;  // one of the positions in string s1
        char c;        // a letter chosen randomly from a string

        System.out.print("Enter a string containing only C, G, T, and A (no blanks allowed, must be all caps): ");
        dnaString = scan.next();
        len = dnaString.length();


        dnaString2 = dnaString.replace('A','T');
        dnaString3 = dnaString2.replace('C','G');
        dnaString4 = dnaString3.replace('G','C');
        dnaString5 = dnaString4.replace('T','A');



        System.out.println(" \nComplement of " +dnaString +" is " + dnaString5);

        location = r.nextInt(len);

        Random rand = new Random();  //makes things random

         c = "ACTG".charAt(rand.nextInt(3));

         dnaString6 = dnaString.substring(0,location) + c + dnaString.substring(location);

         c = "ACTG".charAt(r.nextInt(3));

        System.out.println("Inserting " + c + " at position " + location+ " gives "+ dnaString6);

        dnaString7 = dnaString.substring(0,location) + dnaString.substring(location +1);

        System.out.println("Deleting from " +c +" position " + location+ " gives " + dnaString7);

        dnaString8 = dnaString.substring(0,location) + c + dnaString.substring(location +1);

        System.out.println("Changing position " +location+ " gives " + dnaString8);


     }
}


