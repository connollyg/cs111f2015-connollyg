
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Lab 2
// Date: 09 03 2015
//
// Purpose: To get better at using gvim and to make a java program. Two words of wisodm!
//********************************
import java.util.Date; // needed for printing today's date

public class WordsofWisdom
{
	//------------------
	//main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{ 
	  System.out.println("An apple a day keeps the doctor away. \nEarly to bed and early to rise makes a man healthy, wealthy, and wise.");
	  // Label output with name and date: 
	  System.out.println("Grace Connolly\n Lab 2\n" + new Date() + "\n");
	}
}

