//********************************
// Brianna Crawford and Grace Connolly
// CMPSC 111 Fall 2015
// Lab 6
// Date: 10 01 2015
//
// Purpose: To create a program that allows the user to choose an instrument
// and tempo to create random music.
//********************************

package lab6;

import java.util.Date; // needed for printing today's date
import org.jfugue.player.Player;
import org.jfugue.pattern.Pattern;
import java.util.Scanner;
import java.util.Random;

public class RandomMusic {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 // Label output with name and date:
        System.out.println("Brianna Crawford and Grace Connolly\nLab 6\n" + new Date() + "\n");
    	Scanner input = new Scanner(System.in);	
    	Player player = new Player();
        Random r = new Random();
        
        
        System.out.println("Please type your choice of an instrument.");
        System.out.println("Your choices are violin, choir aahs, ocarina, fx soundtrack, and taiko drum.");
        String instrument = input.nextLine();
        String instrument2 = instrument.toLowerCase();
        
        System.out.println("Please enter your preferred tempo.");
        System.out.println("Your choices are largo, adagio, andantino, allegro, or pretissimo.");
        String tempo = input.nextLine();
        String tempo2 = tempo.toLowerCase();
        
        int song = r.nextInt(3);
        
        System.out.println("Song is: " + song);
        
        if (song == 0){
        	if (instrument2 == "violin"){
        		if(tempo2 == "largo"){
        			Pattern patt1 = new Pattern("T45 I40 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt1);
        		} 
        		else if(tempo2 == "adagio"){
        			Pattern patt2 = new Pattern("T60 I40 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt2);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt3 = new Pattern("T80 I40 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt3);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt4 = new Pattern("T120 I40 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt4);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt5 = new Pattern("T220 I40 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt5);
        		}
        	}
        	else if(instrument2 == "choir aahs"){
        		if(tempo2 == "largo"){
        			Pattern patt6 = new Pattern("T45 I52 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt6);
        		}
        		else if(tempo2 == "adagio"){
        			Pattern patt7 = new Pattern("T60 I52 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt7);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt8 = new Pattern("T80 I52 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt8);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt9 = new Pattern("T120 I52 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt9);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt10 = new Pattern("T220 I52 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt10);
        	    }
        	}
        	else if(instrument2 == "ocarina"){
        		if(tempo2 == "largo"){
        			Pattern patt11 = new Pattern("T45 I79 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt11);
        		}
        		else if(tempo2 == "adagio"){
        			Pattern patt12 = new Pattern("T60 I79 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt12);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt13 = new Pattern("T80 I79 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt13);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt14 = new Pattern("T120 I79 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt14);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt15 = new Pattern("T220 I79 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt15);
        		}
        	}
        	else if(instrument2 == "fx soundtrack"){
        		if(tempo2 == "largo"){
        			Pattern patt16 = new Pattern("T45 I97 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt16);
        		}
        		else if(tempo2 == "adagio"){
        			Pattern patt17 = new Pattern("T60 I97 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt17);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt18 = new Pattern("T80 I97 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt18);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt19 = new Pattern("T120 I97 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt19);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt20 = new Pattern("T220 I97 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt20);
        		}
        	}
        	else if(instrument2 == "taiko drum"){
        		if(tempo2 == "largo"){
        			Pattern patt21 = new Pattern("T45 I116 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt21);
        		}
        		else if(tempo2 == "adagio"){
        			Pattern patt22 = new Pattern("T60 I116 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt22);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt23 = new Pattern("T80 I116 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt23);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt24 = new Pattern("T120 I116 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt24);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt25 = new Pattern("T220 I116 C D E Cw Ah Fh E C A Cw C D E Cw Ah Fh E C A Cw");
        			player.play(patt25);
        		}
        	}
        }
        if (song == 1){
        	if (instrument2 == "violin"){
        		if(tempo2 == "largo"){
        			Pattern patt1 = new Pattern("T45 I40 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt1);
        		} 
        		else if(tempo2 == "adagio"){
        			Pattern patt2 = new Pattern("T60 I40 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt2);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt3 = new Pattern("T80 I40 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt3);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt4 = new Pattern("T120 I40 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt4);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt5 = new Pattern("T220 I40 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt5);
        		}
        	}
        	else if(instrument2 == "choir aahs"){
        		if(tempo2 == "largo"){
        			Pattern patt6 = new Pattern("T45 I52 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt6);
        		}
        		else if(tempo2 == "adagio"){
        			Pattern patt7 = new Pattern("T60 I52 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt7);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt8 = new Pattern("T80 I52 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt8);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt9 = new Pattern("T120 I52 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt9);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt10 = new Pattern("T220 I52 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt10);
        	    }
        	}
        	else if(instrument2 == "ocarina"){
        		if(tempo == "largo"){
        			Pattern patt11 = new Pattern("T45 I79 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt11);
        		}
        		else if(tempo2 == "adagio"){
        			Pattern patt12 = new Pattern("T60 I79 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt12);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt13 = new Pattern("T80 I79 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt13);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt14 = new Pattern("T120 I79 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt14);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt15 = new Pattern("T220 I79 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt15);
        		}
        	}
        	else if(instrument2 == "fx soundtrack"){
        		if(tempo2 == "largo"){
        			Pattern patt16 = new Pattern("T45 I97 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt16);
        		}
        		else if(tempo2 == "adagio"){
        			Pattern patt17 = new Pattern("T60 I97 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt17);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt18 = new Pattern("T80 I97 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt18);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt19 = new Pattern("T120 I97 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt19);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt20 = new Pattern("T220 I97 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt20);
        		}
        	}
        	else if(instrument2 == "taiko drum"){
        		if(tempo2 == "largo"){
        			Pattern patt21 = new Pattern("T45 I116 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt21);
        		}
        		else if(tempo2 == "adagio"){
        			Pattern patt22 = new Pattern("T60 I116 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt22);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt23 = new Pattern("T80 I116 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt23);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt24 = new Pattern("T120 I116 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt24);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt25 = new Pattern("T220 I116 C D E F G A B C D E F G A B C D E F G A B C D E F G A B");
        			player.play(patt25);
        		}
        	}
        } 
        if (song == 2){
        	if (instrument2 == "violin"){
        		if(tempo2 == "largo"){
        			Pattern patt1 = new Pattern("T45 I40 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt1);
        		} 
        		else if(tempo2 == "adagio"){
        			Pattern patt2 = new Pattern("T60 I40 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt2);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt3 = new Pattern("T80 I40 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt3);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt4 = new Pattern("T120 I40 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt4);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt5 = new Pattern("T220 I40 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt5);
        		}
        	}
        	else if(instrument2 == "choir aahs"){
        		if(tempo2 == "largo"){
        			Pattern patt6 = new Pattern("T45 I52 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt6);
        		}
        		else if(tempo2 == "adagio"){
        			Pattern patt7 = new Pattern("T60 I52 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt7);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt8 = new Pattern("T80 I52 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt8);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt9 = new Pattern("T120 I52 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt9);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt10 = new Pattern("T220 I52 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt10);
        	    }
        	}
        	else if(instrument2 == "ocarina"){
        		if(tempo2 == "largo"){
        			Pattern patt11 = new Pattern("T45 I79 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt11);
        		}
        		else if(tempo2 == "adagio"){
        			Pattern patt12 = new Pattern("T60 I79 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt12);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt13 = new Pattern("T80 I79 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt13);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt14 = new Pattern("T120 I79 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt14);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt15 = new Pattern("T220 I79 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt15);
        		}
        	}
        	else if(instrument2 == "fx soundtrack"){
        		if(tempo2 == "largo"){
        			Pattern patt16 = new Pattern("T45 I97 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt16);
        		}
        		else if(tempo2 == "adagio"){
        			Pattern patt17 = new Pattern("T60 I97 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt17);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt18 = new Pattern("T80 I97 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt18);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt19 = new Pattern("T120 I97 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt19);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt20 = new Pattern("T220 I97 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt20);
        		}
        	}
        	else if(instrument2 == "taiko drum"){
        		if(tempo2 == "largo"){
        			Pattern patt21 = new Pattern("T45 I116 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt21);
        		}
        		else if(tempo2 == "adagio"){
        			Pattern patt22 = new Pattern("T60 I116 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt22);
        		}
        		else if(tempo2 == "andantino"){
        			Pattern patt23 = new Pattern("T80 I116 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt23);
        		}
        		else if(tempo2 == "allegro"){
        			Pattern patt24 = new Pattern("T120 I116 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt24);
        		}
        		else if(tempo2 == "pretissimo"){
        			Pattern patt25 = new Pattern("T220 I116 B A E B A E B B B B A A A A A B A E Ew B C Fh");
        			player.play(patt25);
        		}
        	}
        } 


    	 }
    }


  
    	
    
  


