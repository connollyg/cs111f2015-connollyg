
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Lab 3
// Date: 09 10 2015
//
// Purpose: ...
//********************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;
public class TipCalculator
{
	//------------------
	//main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{ 
	  // Label output with name and date: 
	  System.out.println("Grace Connolly Lab 3\n" + new Date() + "\n");
	
	  Scanner scan = new Scanner (System.in);

	  System.out.println("Please enter your name:");
	  String name = scan.next();
		
	  System.out.println( name + ", welcome to the Tip Calculator!");

	  System.out.println("Please enter the amout of your bill:");
	  double bill = scan.nextDouble();

	  System.out.println("Please enter the percentage that you want to tip:");
	  float percentage = scan.nextFloat();

	  double tipamount = bill * percentage;

	  double total = bill + tipamount;

	  System.out.println("Your original bill was $" + bill);
	  System.out.println("Your tip amount is $" + tipamount);
          System.out.println("Your total bill is $" + total);

	  System.out.println("How many people will be splitting the bill?");
	  int people = scan.nextInt();

	  double paythis = bill / people;

	  System.out.println("Each person should pay $" + paythis);
	  System.out.println("Have a nice day! Thank you for using our service.");
	}
}

