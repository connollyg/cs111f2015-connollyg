
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Lab 4
// Date: 09 17 2015
//
// Purpose: ...
//********************************
import java.util.Date; // needed for printing today's date

public class Lab4
{
	//------------------
	//main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{
	  // Label output with name and date:
        System.out.println("Grace Connolly\n Lab 4\n" + new Date() + "\n");

        System.out.println("Check out how that quotation mark appeared");
      // The autocomplete feature is really cool!

	}
}
