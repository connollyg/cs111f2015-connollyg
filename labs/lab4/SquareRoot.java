
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Lab 4
// Date: 09 24 2015
//
// Purpose: To calculate the square root of a given value.
//********************************
import java.util.Date; // needed for printing today's date

import java.util.Scanner;

public class SquareRoot
{
	//------------------
	//main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{
	  // Label output with name and date:
        System.out.println("Grace Connolly\nLab 4\n" + new Date() + "\n");

     Scanner scan = new Scanner (System.in);

     System.out.println("Input a value:");
     double value = scan.nextDouble();    //initial value can be a fraction

     System.out.println("Input a guess for what the square root of your value is.");
     double guess = scan.nextDouble();    //guess can be a fraction

     guess = .5 * (guess + (value/guess));
     double result1 = guess * guess;
     System.out.println("Results of first guess:" + result1);

     System.out.println("Input another guess for the square root.");
     double guess2 = scan.nextDouble();

     guess2 = .5 * (guess2 + (value/guess2));
     double result2 = guess2 * guess2;
     System.out.println("Results of second guess: " + result2);

     System.out.println("Input a final guess for the value of the square root.");
     double guess3 = scan.nextDouble();

     guess3 = .5 * (guess3 + (value/guess3));
     double result3 = guess3 * guess3;
     System.out.println("The original value: " + value);
     System.out.println("The value of the last guess: " + guess3);
     System.out.println("Results of third and final guess: " + result3);


    }
}

