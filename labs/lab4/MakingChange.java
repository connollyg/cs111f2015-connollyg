
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Lab 4
// Date: 09 24 2015
//
// Purpose: To make change for a given amount of money.
//********************************
import java.util.Date; // needed for printing today's date

import java.util.Scanner;

public class MakingChange
{
	//------------------
	//main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{
	  // Label output with name and date:
        System.out.println("Grace Connolly\nLab 4\n" + new Date() + "\n");


    Scanner scan = new Scanner (System.in);

    System.out.println("Enter the amount of money in cents: ");
    int cents = scan.nextInt(); // value to make change for

    int tens;                   //number of ten-dollar bills needed
    int fives;                  //number of five-dollar bills needed
    int ones;                   //number of one-dollar bills needed
    int quarters;               //number of quarters needed
    int dimes;                  //number of dimes needed
    int nickels;                //number of nickels needed
    int pennies;                //number of pennies needed

    System.out.println("To make change for " + cents + " cents, you need:");

    tens = cents / 1000;  //number of $10 bills contained in cents
    cents = cents % 1000; //cents remaining after tens removed

    System.out.print(tens + " tens, ");

    fives = cents / 500;  //number of $5
    cents = cents % 500;  //cents remaining after tens and fives removed

    System.out.print(fives + " fives, ");

    ones = cents / 100;   //number of $1
    cents = cents % 100;  //cents remaining after tens, fives, and ones removed

    System.out.print(ones + " ones, ");

    quarters = cents / 25;  //number of quarters
    cents = cents % 25;     //cents remaining after quarters are also removed

    System.out.print(quarters + " quarters, ");

    dimes = cents / 10;   //number of dimes
    cents = cents % 10;   //cents remaining after dimes are also removed

    System.out.print(dimes + " dimes, ");

    nickels = cents / 5;   //number of nickels
    cents = cents % 5;     //cents remaing after nickels are also removed

    System.out.print(nickels + " nickels, and ");

    pennies = cents / 1;   //number of pennies

    System.out.print(pennies + " pennies.");


    }
}


