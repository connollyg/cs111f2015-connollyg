
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Lab 7
// Date: 10 15 2015
//
// Purpose: To check if rows, columns, and regions in a simple sudoku
// equal 10.
//********************************
import java.util.Date; // needed for printing today's date

public class SudokuMain
{
	//------------------
	//main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{
	  // Label output with name and date:
        System.out.println("Grace Connolly\n Lab 7\n" + new Date() + "\n");

        SudokuChecker sudoku = new SudokuChecker();

        sudoku.getGrid();

        sudoku.checkGrid();
    }
}


