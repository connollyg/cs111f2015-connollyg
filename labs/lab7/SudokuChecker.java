
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Lab 7
// Date: 10 15 2015
//
// Purpose: To create the classes needed for SudokuMain.
//********************************
import java.util.Scanner;

public class SudokuChecker
{

    private int w1, w2, w3, w4, x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4;

    Scanner scan = new Scanner (System.in);

    public SudokuChecker()
    {}

    public void getGrid()
    {
    System.out.println("Welcome to the Sudoku Checker!");
    System.out.println("This program checks simple, small, 4x4 Sudoku grids for correctness.");
    System.out.println("Each column, row, and 2x2 region contains the numbers 1 through 4 only once");
    System.out.println("\nTo check your Sudoku, enter your board one row at a time,");
    System.out.println("with each digit separated by a space. Hit ENTER at the end of a row.");

    System.out.println("Enter Row 1: ");
    w1 = scan.nextInt();
    w2 = scan.nextInt();
    w3 = scan.nextInt();
    w4 = scan.nextInt();
    System.out.println("Enter Row 2: ");
    x1 = scan.nextInt();
    x2 = scan.nextInt();
    x3 = scan.nextInt();
    x4 = scan.nextInt();
    System.out.println("Enter Row 3: ");
    y1 = scan.nextInt();
    y2 = scan.nextInt();
    y3 = scan.nextInt();
    y4 = scan.nextInt();
    System.out.println("Enter Row 4: ");
    z1 = scan.nextInt();
    z2 = scan.nextInt();
    z3 = scan.nextInt();
    z4 = scan.nextInt();
    }

    public void checkGrid()
    {
     //regions
    if (w1 + w2 + x1 + x2 == 10)
        System.out.println("Region 1 is good.");
    else
        System.out.println("Region 1 is wrong.");
    if (w3 + w4 + x3 + x4 == 10)
        System.out.println("Region 2 is good.");
    else
        System.out.println("Region 2 is wrong.");
    if (y1 + y2 + z1 + z2 == 10)
        System.out.println("Region 3 is good.");
    else
        System.out.println("Region 3 is wrong.");
    if (y3 + y4 + z3 + z4 == 10)
        System.out.println("Region 4 is good.");
    else
        System.out.println("Region 4 is wrong.");

    //rows
    if (w1 + w2 + w3 + w4 == 10)
        System.out.println("Row 1 is good.");
    else
        System.out.println("Row 1 is wrong.");
    if (x1 + x2 + x3 + x4 == 10)
        System.out.println("Row 2 is good.");
    else
        System.out.println("Row 2 is wrong.");
    if (y1 + y2 + y3 + y4 == 10)
        System.out.println("Row 3 is good.");
    else
        System.out.println("Row 3 is wrong.");
    if (z1 + z2 + z3 + z4 == 10)
        System.out.println("Row 4 is good.");
    else
        System.out.println("Row 4 is wrong.");

    //columns
    if (w1 + x1 + y1 + z1 == 10)
        System.out.println("Column 1 is good.");
    else
        System.out.println("Column 1 is wrong.");
    if (w2 + x2 + y2 + z2 == 10)
        System.out.println("Column 2 is good.");
    else
        System.out.println("Column 2 is wrong.");
    if (w3 + x3 + y3 + z3 == 10)
        System.out.println("Column 3 is good.");
    else
        System.out.println("Column 3 is wrong.");
    if (w4 + x4 + y4 + z4 == 10)
        System.out.println("Column 4 is good.");
    else
        System.out.println("Column 4 is wrong.");

    }
}

