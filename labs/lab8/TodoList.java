import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class TodoList {

    private ArrayList<TodoItem> todoItems;
    private static final String TODOFILE = "todo.txt";
    //establishes that todo.txt is the todo document we are referencing

    public TodoList() {
        todoItems = new ArrayList<TodoItem>();
    }
    //creates the to do list itself

    public void addTodoItem(TodoItem todoItem) {
        todoItems.add(todoItem);
    }
    //adds a new todo item

    public Iterator getTodoItems() {
        return todoItems.iterator();
    }
    //will look through the items in the todo list

    public void readTodoItemsFromFile() throws IOException {
        Scanner fileScanner = new Scanner(new File(TODOFILE));
        while(fileScanner.hasNext()) {
            String todoItemLine = fileScanner.nextLine();
            Scanner todoScanner = new Scanner(todoItemLine);
            todoScanner.useDelimiter(",");
            String priority, category, task;
            priority = todoScanner.next();
            category = todoScanner.next();
            task = todoScanner.next();
            TodoItem todoItem = new TodoItem(priority, category, task);
            todoItems.add(todoItem);
        }
    }
    //goes through the list, scanning for things like priority, category, or task
    //will sort priority, category, and task when user is creating a new item

    public void markTaskAsDone(int toMarkId) {
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
            TodoItem todoItem = (TodoItem)iterator.next();
            if(todoItem.getId() == toMarkId) {
                todoItem.markDone();
            }
        }
    }
    //marks items as done

    public Iterator findTasksOfPriority(String requestedPriority) {
        ArrayList<TodoItem> priorityList = new ArrayList<TodoItem>();
        // TODO: Add source code that will find and return all tasks of the requestedPriority
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()){
            TodoItem todoItem = (TodoItem)iterator.next();
            if(todoItem.getPriority().equals(requestedPriority)){
                priorityList.add(todoItem);
            }
        }
        return priorityList.iterator();
    }
    //finds and lists the items from the priority level the user requested

    public Iterator findTasksOfCategory(String requestedCategory) {
        ArrayList<TodoItem> categoryList = new ArrayList<TodoItem>();
        // TODO: Add source code that will find and return all tasks for the requestedCategory
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()){
            TodoItem todoItem = (TodoItem)iterator.next();
            if(todoItem.getCategory().equals(requestedCategory)){
                categoryList.add(todoItem);
            }
        }
        return categoryList.iterator();
    }
    //finds and lists the items from the category level the user requested

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
            buffer.append(iterator.next().toString());
            if(iterator.hasNext()) {
                buffer.append("\n");
            }
        }
        return buffer.toString();
    }

}
