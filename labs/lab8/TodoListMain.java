import java.io.IOException;
import java.util.Scanner;
import java.util.Iterator;

public class TodoListMain {

    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to the Todo List Manager!");
        System.out.println("What operation would you like to perform?");
        System.out.println("Available options: read, priority-search, category-search, done, list, quit");

        Scanner scanner = new Scanner(System.in);
        TodoList todoList = new TodoList();

        while(scanner.hasNext()) {
            String command = scanner.nextLine();
            if(command.equals("read")) {
                todoList.readTodoItemsFromFile();
            }
            //will bring up the list
            else if(command.equals("list")) {
                System.out.println(todoList.toString());
            }
            //will list the list
            else if(command.equals("done")) {
                System.out.println("What is the id of the task?");
                int chosenId = scanner.nextInt();
                todoList.markTaskAsDone(chosenId);
            }
            //will mark an item on the list as done
            else if(command.equals("priority")) {
                System.out.println("What is the priority of the task?");
                String chosenPriority = scanner.next();
                Iterator iterator = todoList.findTasksOfPriority(chosenPriority);
                while(iterator.hasNext()){
                System.out.println(iterator.next());
                }
            }
            //will list all the items of the priority the user listed
            else if(command.equals("category")) {
                System.out.println("What is the category of the task?");
                String chosenCategory = scanner.next();
                Iterator iterator = todoList.findTasksOfCategory(chosenCategory);
                while(iterator.hasNext()){
                System.out.println(iterator.next());
                }
            }
            //will list all the items from the category the user listed
            else if(command.equals("quit")) {
                break;
            }
            //quits the program
        }

    }

}
