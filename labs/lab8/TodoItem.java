public class TodoItem {

    private int id;
    private static int nextId = 0;
    private String priority;
    private String category;
    private String task;
    private boolean done;

    public TodoItem(String p, String c, String t) {
        id = nextId;
        nextId++;
        priority = p;
        category = c;
        task = t;
        done = false;
    }
    //created the item strings and categories, this program creates the methods to use in the other two

    public int getId() {
        return id;
    }
    //the method to return the item based on id

    public String getPriority() {
        return priority;
    }
    //the method for if the user searched for a certain priority, return results marked
    //as that priority (A, B, C, etc)

    public String getCategory() {
        return category;
    }
    //the method that returns results marked as a certain category

    public String getTask() {
        return task;
    }
    //the method that returns the task the user searched for

    public void markDone() {
        done = true;
    }
    //will mark a task as completed (true, as opposed to the false for not done)

    public boolean isDone() {
        return done;
    }
    //will return a tasks already completed

    public String toString() {
        return new String(id + ", " + priority + ", " + category + ", " + task + ", done? " + done);
    }
    //for the new tasks created by the user

}
