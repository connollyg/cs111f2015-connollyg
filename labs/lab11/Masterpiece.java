//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.

// Grace Connolly
// CMPSC 111 Fall 2015
// Lab 11
// Date: 11 12 2015
//
// Purpose: To create the drawable objects for MasterpieceMain.
//=================================================
import java.awt.*;
import javax.swing.*;

public class Masterpiece extends JApplet
{
    // instance variables
    private int width, height;
    private Graphics page;

    // constructor
    public Masterpiece(Graphics p, int x, int y)
    {
        width = x;
        height = y;
        page = p;
    }


    // methods

    public void drawBody()
    {
         page.setColor(Color.pink);
         page.fillOval(240,123,95,155);
    }

    public void drawStomach()
    {
        page.setColor(Color.white);
        page.fillOval(262,160,50,80);
    }

    public void drawHead()
    {
        page.setColor(Color.pink);
        page.fillOval(257,76,69,69);
    }

    public void drawEars()
    {
         int x = 252;
         int y = 70;
         int width = 25;
         int height = 25;
         page.fillOval(x,y,width,height);
         page.fillOval(x+52,y,width,height);
    }

    public void drawLimbs()
    {
        int x = 230;
        int y = 120;
        int width = 28;
        int height = 70;
        page.fillOval(x,y,width,height);
        page.fillOval(x+90,y,width,height);
        page.fillOval(x+18,y+122,width,height);
        page.fillOval(x+72,y+122,width,height);
    }

    public void drawEyes()
    {
        page.setColor(Color.black);
        int x = 270;
        int y = 93;
        int width = 10;
        int height = 10;
        page.fillOval(x,y,width,height);
        page.fillOval(x+30,y,width,height);
    }

    public void drawSmile()
    {
        page.drawArc(278,108,30,20,190,160);
    }

    public void drawRain()
    {
        page.setColor(Color.blue);
         int x = 30;
         int y = 60;
         int width = 50;
         int height = 100;
         page.drawLine(x,y,width,height);

         }

    public void drawCloud()
    {
        page.setColor(Color.lightGray);
         int x = 10;
         int y = 20;
         int width = 40;
         int height = 40;
         page.fillOval(x,y,width,height);
         page.fillOval(x+20,y,width,height);
         page.fillOval(x+10,y-10,width,height);

         for (int i = 1; i <= 10; i++)
       { x = x + 60;
         page.fillOval(x,y,width,height);
         page.fillOval(x+20,y,width,height);
         page.fillOval(x+10,y-10,width,height);
         }
    }
}
