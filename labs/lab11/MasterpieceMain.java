//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.

// Grace Connolly
// CMPSC 111 Fall 2015
// Lab 11
// Date: 11 12 2015
//
// Purpose: To create an image using java programming.
//=================================================
import java.awt.*;
import javax.swing.*;

public class MasterpieceMain extends JApplet
{
    //-------------------------------------------------
    // Use Graphics methods to add content to the drawing canvas
    //-------------------------------------------------
    public void paint(Graphics page)
    {

	// create an instance of Masterpiece class
	// NOTE: you may have to pass 'page' in addition to any
	//       other arguments in your constructor or during your method calls
    Masterpiece drawing = new Masterpiece(page, 800, 400);

	// call the methods in the Masterpiece class
    page.setColor(Color.cyan);
    page.fillRect(0,0,700,700);

    drawing.drawBody();
    drawing.drawStomach();
    drawing.drawHead();
    drawing.drawEars();
    drawing.drawLimbs();
    drawing.drawEyes();
    drawing.drawSmile();
    drawing.drawRain();
    drawing.drawCloud();


    // main method that runs the program
    public static void main(String[] args)
    {
        	JFrame window = new JFrame(" Your name ");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new MasterpieceMain());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
		window.setSize(600, 400);

     }
    }
}
