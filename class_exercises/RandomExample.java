//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Lab #
// Date: mm dd yyyy
//
// Purpose: ...
//********************************
import java.util.Date; // needed for printing today's date
import java.util.Random;

public class RandomExample{
    public static void main(String[] args)
    {
        int randomInt;
        float randomFloat;

        Random rand = new Random();

        randomInt = rand.nextInt();
        System.out.println("A random integer :"+randomInt);

        randomInt = rand.nextInt(100);
        System.out.println("A random integer 0-100: "+randomInt);

        randomFloat = rand.nextFloat();
        System.out.println("A random float: "+randomFloat);
    }
}

