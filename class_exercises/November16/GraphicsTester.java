/**
 * A simple class to experiment with Swing graphics
 */

import javax.swing.*;
import java.awt.*;

public class GraphicsTester {
	public static void main(String [] args)
	{
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setLayout(new FlowLayout());
		frame.add(new JButton("I'm a JButton"));
		frame.add(new JTextField("I'm a JTextField"));
		frame.add(new JLabel("I'm a JLabel"));
		frame.add(new JSlider());
		frame.add(new JProgressBar());

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2,2)); // parameters: rows, columns
        panel.add(new JButton("JButton in panel"));
        panel.add(new JLabel("Panel components"));
        panel.add(new JTextField("JTextfield"));
        panel.add(new JButton("Another JButton"));

        frame.add(panel);

        JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayout(3,3));
        panel1.add(new JLabel("okay seriously sliders are cool"));
        panel1.add(new JSlider());
        panel1.add(new JTextField("see wasn't that awesome"));
        panel1.add(new JButton("yes it really was"));
        panel1.add(new JButton("hell yeah!"));

        //frame.add(panel);

		frame.pack();
		frame.setVisible(true);
	}
}
