import java.awt.*;
import javax.swing.*;  // JApplet

public class BasicGraphics extends javax.swing.JApplet
{
    public static void main (String [] args)
    {
        JFrame window = new JFrame("cool graphics");
        window.getContentPane().add(new BasicGraphics());
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);
        window.setSize(600, 400);
    }

    public void paint(Graphics g)
    {
        g.setColor(Color.red);
        g.fillRect(10,20,40,40);
        g.setColor(Color.yellow);
        g.fillOval(100, 100, 300, 300);
    }
}


