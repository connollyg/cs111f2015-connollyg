import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

public class ArrayListExample
{
    public static void main(String args []) throws IOException
    {
        // create an ArrayList to store elements from the file
        ArrayList<String> allWords = new ArrayList<String>();

        // read from the file
        File file = new File("words.txt");
        Scanner scan = new Scanner(file);

        // iterate through the text file and save elements into the list
        while(scan.hasNext())
        {
            allWords.add(scan.next());
        }

        // display the list
        System.out.println(allWords);

        // remove all plural words
        int count = 0;
        while(count < allWords.size())
        {
            String word = allWords.get(count);
            if(word.endsWith("s"))
            {
                allWords.remove(count);
            }
            count++;
        }
    }
}
