import java.io.IOException;
import java.util.Scanner;
import java.util.Iterator;

public class BookListMain
{
    public static void main (String[] args) throws IOException
    {
        System.out.println("Welcome to the Book List! \n You can read, search, exit");

        Scanner scan = new Scanner(System.in);
        BookList booklist = new BookList();

        while(scan.hasNext())
        {
            String input = scan.nextLine();
            if(input.equals("read"))
            {
                booklist.readItemsFromFile();
            }
            else if(input.equals("search"))
            {
                booklist.seach();
            }
            else if(input.equals("exit"))
            {
                break;
            }
        }
    }
}
