//==========================================
// Class Example
// October 26, 2015
//
// Purpose: This program demonstrates the usage of
// the Iterator
//==========================================
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

class IteratorExample {
   public static void main(String args[]) {
	// create an array list
    	ArrayList<String> al = new ArrayList<String>();
	// add elements to the array list
	al.add("Cake");
	al.add("Apple");
	al.add("Candy");
	al.add("Chocolate");
	al.add("Dark Chocolate");
	al.add("Ice Cream");

    System.out.println(al);

	// use iterator to display contents of al
	System.out.print("Original contents of al list: \n ");
	Iterator itr = al.iterator();
	while(itr.hasNext()) {

    		System.out.print(itr.next() + ".\n ");

	}
	System.out.println();


   // iterate through the list without using the Iterator class
   int count = 0;
   while(count < al.size())
   {
        System.out.print(al.get(count));".\n ");
        count++;
   }
}
}
