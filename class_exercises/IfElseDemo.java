
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Lab #
// Date: mm dd yyyy
//
// Purpose: ...
//********************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class IfElseDemo
{
	//------------------
	//main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{
	  // Label output with name and date:
        System.out.println("Your Name\nLab #\n" + new Date() + "\n");

        Scanner input = new Scanner (System.in);
        System.out.print ( "Enter a character to test: " );
        char character;	  	       		// new data type: char
 		character = input.next().charAt(0); 	// get character from input
 	 	if (character == 'a')     	      	// notice ' ' marks char
            System.out.println ( character+ " is a vowel." );
        else if (character == 'e')
            System.out.println ( character+" is a vowel."  );
        else if (character == 'i')
            System.out.println ( character+ " is a vowel." );
        else if (character == 'o')
            System.out.println ( character+ "is a vowel." );
        else if (character == 'u')
            System.out.println ( character+" is a vowel." );
        else
            System.out.println ( character+" is not a vowel." );
    }
}

