//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Class Example
// Date: 11 23 2015
//
// Purpose: To create a program that finds the largest number in a list and then writes that number to a file.
//********************************
import java.util.Date; // needed for printing today's date
import java.io.PrintWriter;
import java.util.*;

public class LargestNumber
{
	//------------------
	//main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{
	  // Label output with name and date:
        System.out.println("Grace Connolly\n Class Example\n" + new Date() + "\n");

        List<Integer> x = new ArrayList<Integer>();
        x.add(1);x.add(2);x.add(3);x.add(4);x.add(5);
        int max = 0;

        try{
            PrintWriter printer = new PrintWriter("LargestNumber.txt");
        for(int num : x){
            System.out.println();
        max = max > num ? max : num;
        }
        printer.print(max);
            }catch(Exception e){
                System.out.println(max);
                }
	}
}


