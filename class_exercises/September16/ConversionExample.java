
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Class Exercise
// Date: 09 16 2015
//
// Purpose: Get the input from the user, manipulate it and output the result.
//********************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;
public class ConversionExample
{
	//------------------
	//main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{ 
	  // Label output with name and date: 
	  System.out.println("Your Name\n" + new Date() + "\n");

	  int num1, num2; 
	  double num3;
	  float num4;
	  
	  Scanner input = new Scanner (System.in);

	  // get the input from the user
	  System.out.println("Enter on integer");
	  num1 = input.nextInt();
	  System.out.println("Enter another integer");
	  num2 = input.nextInt();
	  System.out.println("Enter any number");
	  num3 = input.nextDouble();
	  System.out.println("Enter one more number");
	  num4 = input.nextFloat();

	  // casting example
	  System.out.println("Addition: "+(num1+(int)num3));
	  System.out.println("Addition 2: "+(num1+num3)); //widening
	  System.out.println("Addition 3: "+((int)(num1+num3)));

	  // get the last digit
	  System.out.println("Last digit is: "+(num2%10));
	  System.out.println("Float's last digit is: "+((int)num4%10));

	}
}

