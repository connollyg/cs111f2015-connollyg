
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Class Exercise
// Date: 09 16 2015
//
// Purpose: Get input from the user, manipulate it and output the result.
//********************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;
public class TempuratureConversion
{
	//------------------
	//main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{ 
	  // Label output with name and date: 
	  System.out.println("Your Name\n" + new Date() + "\n");

	  double temp1;

	  Scanner input = new Scanner (System.in);

	  System.out.println("Enter a temperature in F");
	  temp1 = input.netDouble();

	  System.out.println("Temperature in C: "+((temp1 - 32)*5)/9);
	}
}

