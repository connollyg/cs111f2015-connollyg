//==========================================
// Janyl Jumadinova
// Class Exercise
// September 21, 2015
//
// Purpose: This program uses various methods
// of the String class
// ==========================================
 public class StringExample
{

   public static void main(String args[])
   {
      // declare a variable named word of type String
      String word;

      //assign the string to the variable:
      word = "AlexAnder";

      //perform some actions on the string:

      //1. retrieve the length by calling the
      //length method:
      int length = word.length();
      System.out.println("Length: " + length);

      //2. get the last character of the string
      //by calling charAt method
      //
      char letter = word.charAt(length-1);
      System.out.println("Letter: "+letter);

      //3. replace 'A' by 'a' by calling replace
      System.out.println("New word: "+word.replace('A','a'));

      //4. check if two strings are equal to each other
      String word2 = new String("AlexAnder");
      System.out.println("Equals "word.equals(word2));

      //%. get a substring
      System.out.println("Substring: "+word.substring(4));
      System.out.println("Another substring: "+word.substring(0,4));
   }
}
