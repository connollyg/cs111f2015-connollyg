import java.io*;
import java.util.Scanner;

public class BirthdayProblem
{
    public static void main (String args []) throws IOException
    {
        String bd [] = new String [50];
        String used [] = new String[50];

        File file = new File("birthday.txt");
        Scanner input = new Scanner (file);

        int count = 0;
        while(input.hasNext())
        {
            String birthday = input.next();
            bd[count] = birthday;
            count++;
        }

        int i=0, j=0;
        while(i < bd.length)
        {
            while(j<used.length)
            {
                 if(bd[i].equals(used[i]))
                 {
                     System.out.println("Duplicate birthday "+bd[i]);
                     break;
                 }
                 j++;
            }
            used[i]=bd[i];
            System.out.println("birthday "+used[i]);
            i++;
            j=0;
        }
    }
}

