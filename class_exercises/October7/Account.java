
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Class Example October 7
// Date: 10 07 2015
//
// Purpose: ...
//********************************

public class Account
{
    // declare instance variables
    private double balance;

    // create a method that adds value to the balance
    // set method
    public void setBalance(double amount)
    {
        balance += amount;
    }

    // method that returns a value of the balance
    // get method
    public double getBalance()
    {
        return balance;
    }
}

