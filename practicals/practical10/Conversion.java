
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Practical 10
// Date: 11 19 2015
//
// Purpose: To convert time from minutes into hours.
//********************************
import java.util.Date; // needed for printing today's date
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Conversion
{
	//------------------
	//main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{
	  // Label output with name and date:
        System.out.println("Grace Connolly\n Practical 10\n" + new Date() + "\n");

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setBackground(Color.pink);
        final JTextField myTextField = new JTextField("Enter an amount of time in minutes");
        JButton myButton = new JButton("Convert");
        final JLabel conversion = new JLabel("Time in hours: ");

        myButton.addActionListener(new ActionListener()
                {
                    public void actionPerformed(ActionEvent event)
                    {
                       String min = myTextField.getText();
                       int min = Integer.parseInt(min);
                       int hours = min*60;
                       String hours = Integer.toString(hours);
                       conversion.setText(myTextField.getText());
                    }
                });


	}
}

