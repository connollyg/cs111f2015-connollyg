
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Practical 5
// Date: 10 01 2015
//
// Purpose: To use if/else statements and boolean expressions.
//********************************
import java.util.Date; // needed for printing today's date
import java.util.Date;
public class YearChecker
{
	//------------------
	//main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{
	  // Label output with name and date:
        System.out.println("Grace Connolly\nPractical 5\n" + new Date() + "\n");

        Scanner input = new Scanner (System.in);
        System.out.print ("Enter a year between 1000 and 3000: ");
        int year;
        year = scan.nextInt();

        if (year % 4 == 0)
            System.out.println(year+"is a leap year");
        // else if (year == 2013)
         //   System.out.println("It's an emergence year for Brood II of the"
         //          + "17-year cicadas");
      //  else if ( ==)
       //     System.out.println("It's a peak sunspot year.");

        System.out.println("Thank you for using this program!");
	}
}

