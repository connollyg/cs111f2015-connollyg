/************************************
  Honor Code: This work is mine unless otherwise cited.
  Grace Connolly
  CMPSC 111
  24 September 2015
  Practical 4

  Basic Input with a dialog box
 ************************************/

import javax.swing.JOptionPane;

public class Practical4
{
    public static void main ( String[] args )
    {
        Random generator = new Random();
        int surprise;

        // display a dialog with a message
        JOptionPane.showMessageDialog( null, "Let's get you a new identity!" );

        // prompt user to enter the first name
        String name = JOptionPane.showInputDialog(" What is your first name?");

        //create a message with the modified first name
        String newName = "Your new first name is "+ name+"ka";

        //display the message with the modified user's name
        JOptionPane.showMessageDialog(null, newName);

        // TO DO: prompt the user to get the last name, modify the last name and display the new last name

        String lastName = JOptionPane.showInputDialog(null, "What is your last name?");

        String newLastName = "Your new last name is"+ lastName+"bob";

        JOptionPane.showMessageDialog(null, newLastName);

        // prompt user to enter the age
        int ans = Integer.parseInt(JOptionPane.showInputDialog("Enter your age"));

        surprise = generator.nextInt(100);

        // modify the age
        String newAge = "Your age is: "+(ans-surprise); // TO DO: change age using a random number, instead of a constant 10

        // display a message with the new age
        JOptionPane.showMessageDialog(null, newAge);

        // TO DO: come up with your own (at least two) questions and answers

        String MidName = JOptionPane.showInputDialog("What is your middle name?");

        String newMidName = "Your new middle name is "+ MidName+"ia";

        JOptionPane.showMessageDialog(null, newMidName);

        String FavCol = JOptionPane.showInputDialog("What is your favorite color?");

        String newFavCol = "Your new favorite color is "+ FavCol+"urple";

    } //end main
}  //end class Practical4
