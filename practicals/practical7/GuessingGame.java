
//********************************
// Honor Code: This work is mine unless otherwise cited.
// Grace Connolly
// CMPSC 111 Fall 2015
// Practical 7
// Date: 10 22 2015
//
// Purpose: To create a program that has the user guess a number
// between 1 and 100, informing them if the answer is higher or lower.
//********************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class GuessingGame
{
	//------------------
	//main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{
	  // Label output with name and date:
        System.out.println("Grace Connolly\n Practical 7\n" + new Date() + "\n");

        int number, answer;
        int numTries = 1;

        Scanner input = new Scanner (System.in);

        System.out.println("Guess an integer between 1 and 100.");
        answer = 66;
        number = input.nextInt();

        while (number != answer)
        {
        if (number > answer)
           System.out.println("Too high! Guess again.");
        else if (number < answer)
           System.out.println("Too low! Guess again.");
        number = input.nextInt();
        numTries++;
        }

        if (number == answer)
        System.out.println("You guessed correctly! Congratulations.");

        System.out.println("You guessed this many times: " + numTries++);

    }
}

