//***********************
//Practical 2
//3 September 2015
// Prints a sun
//Honor Code: This work is mine unless otherwise cited.
//***********************
import java.util.Date;
public class PrintSun
{
	public static void main(String[] args)
	{
	 System.out.println("Grace Connolly, CMPSC 111\n" +new Date() + "\n");
	 System.out.println(" \\   |   /");
	 System.out.println(" \\ \\  |  / /");
	 System.out.println("  \\ oooooo/");
	 System.out.println("---oooooooo---");
	 System.out.println("    oooooo");
	 System.out.println("   /  |  \\ ");
	 System.out.println("  /   |   \\ ");
	}
}
