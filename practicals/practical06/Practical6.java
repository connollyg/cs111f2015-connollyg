//====================================
// CMPSC 111
// Practical 6
// 15--16 October 2015
//
// This program describes an octopus in the kitchen.
//====================================

import java.util.Date;

public class Practical6
{
    public static void main(String[] args)
    {
        System.out.println("Janyl Jumadinova\n" + new Date() + "\n");

        // Variable dictionary:
        Octopus ocky, arthur;           // an octopus
        Utensil spat, spoon;           // a kitchen utensil

        spat = new Utensil("spatula"); // create a spatula
        spat.setColor("green");        // set spatula properties--color...
        spat.setCost(10.59);           // ... and price

        spoon = new Utensil("spoon");
        spoon.setColor("pink");
        spoon.setCost(108.77);

        ocky = new Octopus("Ocky", 10);    // create and name the octopus
        ocky.setWeight(100);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil.

        arthur = new Octopus("Arthur", 9);
        arthur.setWeight(95);
        arthur.setUtensil(spoon);

        System.out.println("Testing 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight()
            + " pounds\n" + "and is " + ocky.getAge()
            + " years old. His favorite utensil is a "
            + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $"
            + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());

        System.out.println(arthur.getName() + " weighs " +arthur.getWeight()
            + " pounds\n" + "and is " + arthur.getAge()
            + " years old. His favorite utensil is a "
            + arthur.getUtensil());

        System.out.println(arthur.getName() + "'s " + arthur.getUtensil() + " costs $"
            + arthur.getUtensil().getCost());
        System.out.println("Utensil's color: " + spoon.getColor());

        // Use methods to change some values:

        ocky.setAge(20);
        ocky.setWeight(125);
        spat.setCost(15.99);
        spat.setColor("blue");

        arthur.setAge(14);
        arthur.setWeight(400);
        spoon.setCost(16.89);
        spoon.setColor("silver");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(ocky.getName() + "'s new age: " + ocky.getAge());
        System.out.println(ocky.getName() + "'s new weight: " + ocky.getWeight());
        System.out.println("Utensil's new cost: $" + spat.getCost());
        System.out.println("Utensil's new color: " + spat.getColor());

        System.out.println(arthur.getName() + "'s new age: " + arthur.getAge());
        System.out.println(arthur.getName() + "'s new weight: " + arthur.getWeight());
        System.out.println("Utensil's new cost: $" + spoon.getCost());
        System.out.println("Utensil's new color: " + spoon.getColor());
    }
}
