//*********************************************************************************
// CMPSC 111 Fall 2015
// Practical 9
//
// Purpose: Program demonstrating reading input from the terminal
//*********************************************************************************
import java.util.Scanner;

public class SwitchDay
{
    public static void main(String[] args)
    {
        String day;
        Scanner s = new Scanner(System.in);
        System.out.print( "Enter a day of the week: "  );
        day = s.nextLine();


    // TO DO: Using a switch statement, given the day of the week, print whether it is a weekday or a weekend day.
    switch (day)
     {
        case Monday:
           System.out.println("is a weekday.");
        break;
        case Tuesday:
           System.out.println("is a weekday.");
        break;
        case Wednesday:
           System.out.println("is a weekday.");
        break;
        case Thursday:
           System.out.println("is a weekday.");
        break;
        case Friday:
           System.out.println("is a weekday.");
        break;
        case Saturday:
            System.out.println("is a weekend day.");
        break;
        case Sunday:
            System.out.println("is a weekend day.");
        break;

    }
    }
}
